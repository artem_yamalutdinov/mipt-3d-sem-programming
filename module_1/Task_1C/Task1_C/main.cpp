//
//  main.cpp
//  Task1_C
//
//  Created by Артем Ямалутдинов on 27.09.2018.
//  Copyright © 2018 Артем Ямалутдинов. All rights reserved.
//

#include <array>
#include <iostream>
#include <string>
#include <vector>

// Класс, отвечающий конечному автомату, построенному на боре
class CTrie {
public:
    explicit CTrie(std::string_view mask);
//    Добавление строки в бор
    void add(std::string_view, const int&);
//    Поиск вхождкений шаблона в данную строку
    std::vector<int> findEntries(std::string_view);
private:
//    Константа, определяющая размер алфавита
    static const int alphabetSize = 26;
//    Класс, отвечающий вершине бора
    class CTrieNode {
    public:
        CTrieNode(int uParent, char cParentletter): parent(uParent), parentLetter(cParentletter),
        suffixLink(-1), fastSuffixLink(-1), isLeaf(false) {
            sons.fill(-1);
            transitions.fill(-1);
        }
//        Родительская вершина
        int parent;
//        Суффиксная ссылка
        int suffixLink;
//        Сжатая суффиксная ссылка
        int fastSuffixLink;
//        Массив потомков (-1, если нет ребра)
        std::array<int, alphabetSize> sons;
//        Массив переходов автомата (-1, если нет перехода)
        std::array<int, alphabetSize> transitions;
//        Индексы подстрок, заканчивающихся в данной вершине
        std::vector<int> substringIndices;
        char parentLetter;
//        Флаг, обозначающий, является ли вершина терминальной
        bool isLeaf;
    };
//    Метод перехода в автомате
    int getLink(int, char);
//    Метод перехода по суффиксной ссылке
    int getSuffixLink(int);
//    Метод перехода по сжатой суффиксной ссылке
    int getFastSuffixLink (int);
//    Бор (хранится как вектор вершин)
    std::vector<CTrieNode> trie;
//    Вектор пар (начало подшаблона между ?, конец подшаблона между ?)
    std::vector<std::pair<int, int>> maskPatterns;
//    Маска, по которой происходит поиск
    std::string sMask;
    
};

CTrie::CTrie(std::string_view mask): sMask(mask), trie({CTrieNode(-1, '\0')}) {
    std::pair<int, int> pattern{0, 0};
//    Выделяем подшаблоны из маски
    if (mask[1] == '?' && isalpha(mask[0])) {
        pattern.second = 0;
        maskPatterns.push_back(pattern);
    }
    for (int i = 1; i < mask.length() - 1; ++i) {
        if (mask[i - 1] == '?' && isalpha(mask[i])) {
            pattern.first = i;
        }
        if (mask[i + 1] == '?' && isalpha(mask[i])) {
            pattern.second = i;
            maskPatterns.push_back(pattern);
        }
    }
    if (mask[mask.length() - 2] == '?' && isalpha(mask[mask.length() - 1])) {
        pattern.first = mask.length() - 1;
    }
    if (isalpha(mask[mask.length() - 1])) {
        pattern.second = mask.length() - 1;
        maskPatterns.push_back(pattern);
    }
//    Добавляем найденные подшаблоны в бор
    int index = 0;
    for (auto&& pattern: maskPatterns) {
        add(mask.substr(pattern.first, pattern.second - pattern.first + 1), index);
        ++index;
    }
}

// Методы getLink и getSuffixLink рекурсивно вызывают друг друга
int CTrie::getLink(int index, char symbol) {
    if (trie[index].transitions[symbol] == -1) {
        if (trie[index].sons[symbol] != -1) {
            trie[index].transitions[symbol] = trie[index].sons[symbol];
        }
        else {
            if (index == 0) {
                trie[index].transitions[symbol] = 0;
            }
            else {
//                Переходим в потомка суффиксной ссылки с данным символом (если такой потомок есть)
                trie[index].transitions[symbol] = getLink(getSuffixLink(index), symbol);
            }
        }
    }
    return trie[index].transitions[symbol];
}

int CTrie::getSuffixLink(int index) {
    if (trie[index].suffixLink == -1) {
        if (index == 0 || trie[index].parent == 0) {
            trie[index].suffixLink = 0;
        }
        else {
//            Идем в потомка суффиксной ссылки родителя (если такой потомок есть)
            trie[index].suffixLink = getLink(getSuffixLink(trie[index].parent), trie[index].parentLetter);
        }
    }
    return trie[index].suffixLink;
}

int CTrie::getFastSuffixLink (int index) {
    if (trie[index].fastSuffixLink == -1) {
        int suffLink = getSuffixLink(index);
        if (index == 0 || suffLink == 0) {
            trie[index].fastSuffixLink = 0;
        }
//        Переходим по суффиксным ссылкам до тех пор, пока не придем в терминальную вершину
        else if (trie[suffLink].isLeaf) {
            trie[index].fastSuffixLink = suffLink;
        }
        else {
            trie[index].fastSuffixLink = getFastSuffixLink(suffLink);
        }
    }
    return trie[index].fastSuffixLink;
}


void CTrie::add (std::string_view pattern, const int& index) {
    int currentNode = 0;
    for (auto&& letter: pattern) {
//        Ищем номер буквы в алфавите
        char letterValue = letter - 'a';
//        Если есть ребро, переходим по нему
        if (trie[currentNode].sons[letterValue] == -1) {
            trie[currentNode].sons[letterValue] = trie.size();
            trie.emplace_back(currentNode, letterValue);
        }
        currentNode = trie[currentNode].sons[letterValue];
    }
//    По окончании цикла попадаем в терминальную вершину
    trie[currentNode].isLeaf = true;
    trie[currentNode].substringIndices.push_back(index);
}

std::vector<int> CTrie::findEntries(std::string_view text) {
    int currentNode = 0;
//    Вектор, хранящий число подстрок, которые могут войти в текст на данной позиции
    std::vector<int> res(text.length(), 0);
    for (int i = 0; i < text.length(); ++i) {
//        Переходим по ребру в потомка
        currentNode = getLink(currentNode, text[i] - 'a');
//        Пробегаемся по сжатым суффиксным ссылкам, ища вхождения
        for (int j = currentNode; j != 0; j = getFastSuffixLink(j)) {
            for (auto&& index: trie[j].substringIndices) {
//                Отмечаем позицию, с которой шаблон начинает входить в строку
                int startOfEntry = i - maskPatterns[index].second;
//                Если начало и конец шаблона помещаются внутри строки, увеличиваем счетчик вхождений
                if (startOfEntry >= 0 && startOfEntry + sMask.size() - 1 < res.size()) {
                        res[startOfEntry]++;
                }
            }
        }
    }
    std::vector<int> answer;
//    Вхождение всех подшаблонов равносилно вхождению шаблона в строку целиком, так что ищем такие i, что res[i]
//    равно числу подшаблонов
    for (int i = 0; i < res.size(); ++i) {
        if (res[i] == maskPatterns.size()) {
            std::cout << i << std::endl;
        }
    }
    return answer;
}


int main() {
    std::ios_base::sync_with_stdio(false);
    std::string pattern, text;
    std::cin >> pattern >> text;
    CTrie trie(pattern);
    std::vector<int> ans = trie.findEntries(text);
    return 0;
}
