//
//  main.cpp
//  Task1_B2
//
//  Created by Артем Ямалутдинов on 03.10.2018.
//  Copyright © 2018 Артем Ямалутдинов. All rights reserved.
//

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

//  Построение префикс-функции по строке
std::vector<size_t> prefix_function(std::string_view sText) {
//    Массив, в котором хранятся значения функции (считаем, что prefix[0] = 0)
    std::vector<size_t> aPrefixValues(sText.length(), 0);
    for (size_t i = 1; i < sText.length(); ++i) {
        size_t ulIndex = aPrefixValues[i - 1];
        while (ulIndex != 0 && (sText[i] != sText[ulIndex])) {
//            Идем в глубь массива, пока не встретим символ, совпадающий с исходным
            ulIndex = aPrefixValues[ulIndex - 1];
        }
        if (sText[i] == sText[ulIndex]) {
            ++ulIndex;
        }
        aPrefixValues[i] = ulIndex;
    }
    return aPrefixValues;
}

// Построение z-функции по строке
std::vector<size_t> z_function(const std::string_view sText) {
    std::vector<size_t> aZValues(sText.length(), 0);
    aZValues[0] = sText.length();
//    Будем сохранять левую и правую границу z-блока с максимальной правой границей
    size_t ulLeftBound = 0, ulRightBound = 0;
    for (size_t i = 0; i < sText.length(); ++i) {
//        Сравним значения z[i - left] и right - i. Если right - i > 0, то минимум из этих значений –
//        минимальное возможное значение z-функции в точке. Иначе иницализируем z[i] нулем.
        aZValues[i] = std::max(0, std::min(static_cast<int>(ulRightBound - i),
                                           static_cast<int>(aZValues[i - ulLeftBound])));
//        Если необходимо, пробегаемся по строке от правого края, увеличивая значение функции
        while (i + aZValues[i] < sText.length() && sText[aZValues[i]] == sText[i + aZValues[i]]) {
            ++aZValues[i];
        }
//        Обновляем значение левого и правого края
        if (i + aZValues[i] > ulRightBound) {
            ulLeftBound = i;
            ulRightBound = i + aZValues[i];
        }
    }
    return aZValues;
}

//  Построение префикс-функции по z-функции
std::vector<size_t> build_prefix_from_z (const std::vector<size_t>& aZFunction) {
//    Вектор, в который будет записан ответ
    std::vector<size_t> aPrefixFunction(aZFunction.size(), 0);
    for (size_t i = 1; i < aZFunction.size(); ++i) {
//        Пробегаемся по строке справа налево от z[i]
//        (можно заметить, что значение префикс-функции в позиции i + j не меньше, чем j + 1)
        for (long j = aZFunction[i] - 1; j >= 0; --j) {
//            Если встретили уже вычисленное значение, выходим из цикла
            if (aPrefixFunction[i + j] != 0) {
                break;
            }
//            Иначе записываем j + 1
            else {
                aPrefixFunction[i + j] = j + 1;
            }
        }
    }
    return aPrefixFunction;
}

//  Построение z-функции по префикс-функции
std::vector<size_t> build_z_from_prefix (const std::vector<size_t>& aPrefixFunction) {
//  Вектор, в который будет записан ответ
    std::vector<size_t> aZFunction(aPrefixFunction.size(), 0);
//    Записываем в z-функцию максимально возможное значение
//    В цикле добавляем сравнение с размером строки, чтобы избежать выхода за пределы
    for (size_t i = 1; i < aPrefixFunction.size() && i - aPrefixFunction[i] + 1 < aPrefixFunction.size(); ++i) {
        aZFunction[i - aPrefixFunction[i] + 1] = aPrefixFunction[i];
    }
//    Считаем, что z[0] = размеру строки
    aZFunction[0] = aPrefixFunction.size();
    size_t i = 1;
//
    while (i < aPrefixFunction.size()) {
        size_t tmp = i;
//        Если оказываемся внутри z-блока, обновляем значение функции
        if (aZFunction[i] > 0) {
//            Проходимся по блоку, пока не найдем значение, большее z[i]
            for (size_t j = 1; j < aZFunction[i]; ++j) {
                if (aZFunction[i + j] > aZFunction[i]) {
                    break;
                }
                aZFunction[i + j] = std::min(aZFunction[i] - j, aZFunction[j]);
                tmp = i + j;
            }
        }
        i = tmp + 1;
    }
    return aZFunction;
}

// Построение лексикографически минимальной строки по z-функции
std::string build_string_from_z(const std::vector<size_t>& aZFunction) {
    if (aZFunction.empty()) {
        return "";
    }
//    Из условия лексикографической минимальности строка должна начинаться с "a"
    std::string sAnswer = "a";
//    Храним в векторе позиции, на которых стоят буквы, которые нельзя добавить в строку
//    (иначе значение z-функции увеличится)
    std::vector<size_t> aUsedLettersPos;
//    Номер позиции, которую мы записываем в строку
    size_t ulPos = 1;
//    Номер символа внутри максимального префикса, который мы записываем
    size_t ulPrefixPos = 0;
    while (ulPos < aZFunction.size()) {
//        Если z-функция равна нулю и мы не находимся внутри z-блока, записываем первый допустимый символ
//        (первый символ, который не совпадает ни с каким из вектора)
        if (aZFunction[ulPos] == 0 && ulPrefixPos == 0) {
            char cCurrentLetter = 'b';
            while (std::any_of(aUsedLettersPos.begin(), aUsedLettersPos.end(),
                               [cCurrentLetter, &sAnswer](size_t i) {return sAnswer[i] == cCurrentLetter;})) {
                ++cCurrentLetter;
            }
            sAnswer += cCurrentLetter;
            aUsedLettersPos.clear();
            ++ulPos;
        }
//        Иначе начинаем записывать z-блок, начинающийся в данном символе
        else {
            size_t ulPrefixSize = aZFunction[ulPos];
            ulPrefixPos = 0;
            while (ulPrefixSize > 0) {
//                Если мы встречаем z-блок с большей правой границей, обновляем размер блока
                if (aZFunction[ulPos] > ulPrefixSize) {
                    ulPrefixSize = aZFunction[ulPos];
                    aUsedLettersPos.push_back(aZFunction[ulPos]);
                    ulPrefixPos = 0;
                }
//                Если мы встречаем  z-блок с такой же правой границей, записываем его последний символ как использованный
                if (aZFunction[ulPos] == ulPrefixSize) {
                    aUsedLettersPos.push_back(aZFunction[ulPos]);
                }
//                Дописываем символ в строку и обновляем размеры блока и позицию внутри него
                sAnswer += sAnswer[ulPrefixPos++];
                --ulPrefixSize;
                ++ulPos;
            }
//            Обнуляем значение позиции, чтобы сигнализировать, что мы вышли из z-блока
            ulPrefixPos = 0;
        }
    }
    return sAnswer;
}

// Построение лексикографически минимальной строки по префикс-функции
std::string build_string_from_prefix(const std::vector<size_t>& aPrefixFunction) {
    if (aPrefixFunction.empty()) {
        return "";
    }
//    Из условия лексикографической минимальности строка должна начинаться с "a"
    std::string sAnswer = "a";
//    Храним в векторе позиции, на которых стоят буквы, которые нельзя добавить в строку
//    (иначе значение префикс-функции увеличится)
    std::vector<size_t> aUsedLettersPos;
    for (size_t i = 1; i < aPrefixFunction.size(); ++i) {
//        Если префикс-функция равна 0, спускаемся по строке, определяя символы, которые нельзя добавить
        if (aPrefixFunction[i] == 0) {
            size_t j = aPrefixFunction[i - 1];
            while (j > 0) {
                aUsedLettersPos.push_back(j);
                j = aPrefixFunction[j - 1];
            }
            char cCurrentSymbol = 'b';
            while (std::any_of(aUsedLettersPos.begin(), aUsedLettersPos.end(),
                               [cCurrentSymbol, &sAnswer](size_t i){return sAnswer[i] == cCurrentSymbol;})) {
                ++cCurrentSymbol;
            }
            sAnswer += cCurrentSymbol;
            aUsedLettersPos.clear();
        }
//        Иначе прибавляем к строке соответствующий символ
        else {
            sAnswer += sAnswer[aPrefixFunction[i] - 1];
        }
    }
    return sAnswer;
}

int main() {
    std::vector<size_t> z_function;
    size_t lCurrentValue = 0;
    std::vector<size_t> prefix_function;
    while (std::cin >> lCurrentValue) {
        z_function.push_back(lCurrentValue);
    }
    std::string sAnswer = build_string_from_z(z_function);
    std::cout << sAnswer << std::endl;
    return 0;
}

