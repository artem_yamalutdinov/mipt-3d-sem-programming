//
//  main.cpp
//  Task_1A
//
//  Created by Артем Ямалутдинов on 03.10.2018.
//  Copyright © 2018 Артем Ямалутдинов. All rights reserved.
//


#include <array>
#include <iostream>
#include <string>
#include <vector>

class CSubstringFinder {
public:
    explicit CSubstringFinder(std::string_view sTemplate): m_sPattern(sTemplate), m_uLeftBound(0), m_uRightBound(0) {
        //        Считаем Z-функцию для шаблона
        m_aTemplateValues.assign(m_sPattern.length(), 0);
        for (size_t i = 1; i < m_sPattern.length(); ++i) {
            m_aTemplateValues[i] = std::max(0, std::min(static_cast<int>(m_uRightBound - i),
                                                        static_cast<int>(m_aTemplateValues[i - m_uLeftBound])));
            while (i + m_aTemplateValues[i] < m_sPattern.length()
                   && m_sPattern[m_aTemplateValues[i]] == m_sPattern[m_aTemplateValues[i] + i])
            {
                ++m_aTemplateValues[i];
            }
            if (i + m_aTemplateValues[i] == m_uRightBound) {
                m_uLeftBound = i;
                m_uRightBound = i + m_aTemplateValues[i];
            }
        }
        m_uLeftBound = 0;
        m_uRightBound = 0;
    };
    
    //    Главный метод, принимающий строку и выводящий позиции подстрок
    //    (чтобы ускорить выполнение, будем записывать ответ в буферный массив и выводить его тогда,
    //     когда заполнится буфер или окончится ввод)
    void findSubstrings(std::string_view sText, std::ostream& out) {
        size_t uBufferIdx = 0;
        for (size_t i = 0; i < sText.length(); ++i) {
            size_t uCurrentValue = zFunctionValue(sText, i);
            if (uCurrentValue == m_sPattern.length()) {
                m_aBuffer[uBufferIdx++] = i;
            }
            if (uBufferIdx == g_uBufferSize) {
                printBuffer(out, uBufferIdx);
            }
        }
        
        printBuffer(out, uBufferIdx);
    }
    
private:
    //    Константа, определяющая размер буфера, который мы выводим
    static const size_t g_uBufferSize = 100;
    //    Границы Z-блока c максимальной правой границей
    size_t m_uLeftBound;
    size_t m_uRightBound;
    //    Шаблон, который ищется в строке
    const std::string m_sPattern;
    //    Значения префикс-функции для символов шаблона
    std::vector<size_t> m_aTemplateValues;
    //    Буфер, в котором сохраняется ответ
    std::array<size_t, g_uBufferSize> m_aBuffer;
    
    //    Вывод ответа из буфера
    void printBuffer(std::ostream& out, size_t& nSymbols) {
        for (size_t i = 0; i < nSymbols; ++i) {
            out << m_aBuffer[i] << '\n';
        }
        nSymbols = 0;
        m_aBuffer.fill(0);
    }
    
    //    Z-функция для конкретного символа
    size_t zFunctionValue (std::string_view sText, size_t uIndex) {
        size_t uValue = std::max(0, std::min(static_cast<int>(m_uRightBound - uIndex),
                                             static_cast<int>(m_aTemplateValues[uIndex - m_uLeftBound])));
        while (uIndex + uValue < sText.length() && m_sPattern[uValue] == sText[uIndex + uValue]) {
            ++uValue;
        }
        if (uValue + uIndex > m_uRightBound) {
            m_uLeftBound = uIndex;
            m_uRightBound = uValue + uIndex;
        }
        return uValue;
    }
};


int main() {
    std::string sTemplate, sText;
    std::cin >> sTemplate >> sText;
    CSubstringFinder find(sTemplate);
    find.findSubstrings(sText, std::cout);
    return 0;
}
