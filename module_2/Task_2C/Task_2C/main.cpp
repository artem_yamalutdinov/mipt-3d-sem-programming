//
//  main.cpp
//  Task_2C
//
//  Created by Артем Ямалутдинов on 29/10/2018.
//  Copyright © 2018 Артем Ямалутдинов. All rights reserved.
//

#include <array>
#include <iostream>
#include <string>
#include <vector>

// Класс, хранящий суффиксный массив строки
class CSuffixArray {
public:
    explicit CSuffixArray(std::string_view text): m_arraySize(text.length()), m_classNumbers(m_arraySize, 0),
    m_lcpNeighbours(m_arraySize, 0), m_inversedSuffArray(m_arraySize, 0), m_separatorPos(-1)
    {
        buildArray(text);
    }
    // При построении массива от конкатенации двух строк передаем позицию разделяющего символа
    CSuffixArray(std::string_view text, size_t firstLen): m_arraySize(text.length()),
    m_classNumbers(m_arraySize, 0),m_lcpNeighbours(m_arraySize, 0), m_inversedSuffArray(m_arraySize, 0),
    m_separatorPos(firstLen)
    {
        buildArray(text);
    }
    
    const size_t& operator[] (size_t position) const {
        return m_suffArray[position];
    }
    
    const size_t& lcp(size_t position) const {
        return m_lcpNeighbours[position];
    }
    
    size_t size() const {
        return m_arraySize;
    }
    
    size_t separator() const {
        return m_separatorPos;
    }
    
private:
    static const size_t ALPHABET_SIZE = 255;
    // Позиция символа разделяющего две строки (-1, если массив строится по одной строке)
    size_t m_separatorPos;
    //    Размер суффиксного массива
    const size_t m_arraySize;
    //    Массив номеров классов эквивалентности, к которым принадлежат соответствующие символы строки
    std::vector<size_t> m_classNumbers;
    //    Суффиксный массив
    std::vector<size_t> m_suffArray;
    //    Массив, обратный суффиксному (такой массив, что inversedSuffArray[suffArray[i]] = i)
    std::vector<size_t> m_inversedSuffArray;
    //    Массив, хранящий LCP i-ой и (i+1)-ого в лексикографическом порядке суффиксов
    std::vector<size_t> m_lcpNeighbours;
    // Метод построения суффиксного массива
    void buildArray(std::string_view text) {
        size_t n_classes = 0;
        std::vector<size_t> newClassNumbers(m_arraySize, 0);
        //    На нулевом шаге алгоритма сортируем все символы строки
        m_suffArray = countingSort(text, n_classes);
        //    Храним новую перестановку суффиксов
        std::vector<size_t> newPermutation(m_arraySize, 0);
        //    Сортируем подстроки размера 2^j, пока они не превышают размер строки
        for (size_t j = 0; (1 << j) < m_arraySize; ++j) {
            //        Сдвигаем результат, полученный для подстрок размера  2^(j - 1)
            for (size_t i = 0; i < m_arraySize; ++i) {
                long sortedValue = m_suffArray[i] - (1 << j);
                //            Если полученное значение отрицательное, выполняем циклический сдвиг
                newPermutation[i] = sortedValue >= 0 ? sortedValue : sortedValue + m_arraySize;
            }
            //        Выполняем сортировку подсчетом
            std::vector<size_t> count(n_classes, 0);
            for (auto&& symbol: newPermutation) {
                ++count[m_classNumbers[symbol]];
            }
            for (size_t i = 1; i < n_classes; ++i) {
                count[i] += count[i - 1];
            }
            //        Записываем полученную перестановку в SuffArray
            for (long i = m_arraySize - 1; i >= 0 ; --i) {
                m_suffArray[--count[m_classNumbers[newPermutation[i]]]] = newPermutation[i];
            }
            //        Обновляем номера классов для новой перестановки
            newClassNumbers[m_suffArray[0]] = 0;
            n_classes = 1;
            for (size_t i = 1; i < m_arraySize; ++i) {
                size_t firstMiddle = (m_suffArray[i] + (1 << j)) % m_arraySize;
                size_t secondMiddle = (m_suffArray[i - 1] + (1 << j)) % m_arraySize;
                if (m_classNumbers[m_suffArray[i]] != m_classNumbers[m_suffArray[i - 1]] ||
                    m_classNumbers[firstMiddle] != m_classNumbers[secondMiddle]) {
                    ++n_classes;
                }
                newClassNumbers[m_suffArray[i]] = n_classes - 1;
            }
            m_classNumbers = newClassNumbers;
        }
        kasaiLCP(text);
    }
    //    Карманная сортировка подсчетом
    std::vector<size_t> countingSort(std::string_view text, size_t& n_classes) {
        std::vector<size_t> count(ALPHABET_SIZE, 0);
        std::vector<size_t> result(m_arraySize, 0);
        for (auto&& symbol: text) {
            ++count[symbol];
        }
        for (size_t i = 1; i < count.size(); ++i) {
            count[i] += count[i - 1];
        }
        for (size_t i = 0; i <m_arraySize; ++i) {
            result[--count[text[i]]] = i;
        }
        m_classNumbers[result[0]] = 0;
        for (size_t i = 1; i <m_arraySize; ++i) {
            if (text[result[i]] != text[result[i - 1]]) {
                ++n_classes;
            }
            m_classNumbers[result[i]] = n_classes - 1;
        }
        return result;
    }
    //    Подсчет LCP (алгоритм Касаи)
    void kasaiLCP(std::string_view text) {
        //    Заполняем массив inversedSuffArray
        for (size_t i = 0; i < m_arraySize; ++i) {
            m_inversedSuffArray[m_suffArray[i]] = i;
        }
        //    Индекс символа, начиная с которого мы перебираем подстроки
        size_t startIndex = 0;
        //    Проходимся по всем суффиксам в лексикографическом порядке
        for (size_t i = 0; i < m_arraySize; ++i) {
            if (startIndex > 0) {
                --startIndex;
            }
            //        Записываем LCP для подстроки '#' в конец массива
            if (m_inversedSuffArray[i] == m_arraySize - 1) {
                m_lcpNeighbours[m_arraySize - 1] = 0;
                startIndex = 0;
            }
            else {
                size_t nextSuffix = m_suffArray[m_inversedSuffArray[i] + 1];
                //            Перебираем символы текущего суффикса, пока они совпадают с соответсвующими
                //            символами следующего суффикса
                while (std::max(i + startIndex, startIndex + nextSuffix) < m_arraySize &&
                       text[i + startIndex] == text[nextSuffix + startIndex]) {
                    ++startIndex;
                }
                m_lcpNeighbours[m_inversedSuffArray[i]] = startIndex;
            }
        }
    }
};

std::string kCommonSubstring(const std::string firstStr, const std::string secondStr, size_t substrNum) {
    std::string concatenatedStr = firstStr + '#' + secondStr + '$';
    CSuffixArray arr(concatenatedStr, firstStr.length());
    //    Счетчик числа рассмотренных подстрок
    size_t substrCnt = 0;
    //    Минимальное значение LCP среди подряд идущих суффиксов одной строки
    size_t minLCP = 0;
    for (size_t i = 2; i < arr.size() - 1; ++i) {
        //        Проверяем, принадлежат ли суффиксы разным строке
        if ((arr[i] <= arr.separator()) != (arr[i + 1] <= arr.separator())) {
            //            Каждый такой суффикс добавляет LCP[i] - minLCP уникальных общих подстрок
            if (arr.lcp(i) > minLCP) {
                //                Обновляем счетчик
                substrCnt += arr.lcp(i) - minLCP;
            }
            //            Если значение счетчика больше требуемого, выводим подстроку
            if (substrCnt >= substrNum) {
                return concatenatedStr.substr(arr[i], substrNum - substrCnt + arr.lcp(i));
            }
            //            Обновляем значение minLCP (т.к мы перешли к суффиксам другой строки)
            minLCP = arr.lcp(i);
        }
        else {
            //            Если суффиксы принадлежат одной строке, то просто обновляем значение minLCP
            minLCP = std::min(minLCP, arr.lcp(i));
        }
    }
    //    Если счетчик не дошел до нужного числа подстрок, выводим -1
    return "-1";
}

int main() {
    std::string firstString, secondString;
    size_t substrNum;
    std::cin >> firstString >> secondString >> substrNum;
    std::cout << kCommonSubstring(firstString, secondString, substrNum) << std::endl;
    return 0;
}

