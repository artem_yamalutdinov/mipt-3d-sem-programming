#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <limits>

// Класс, реализующий основные операции с векторами и точками в 2D и 3D
class CPointVector {
public:
    // Конструктор для трехмерного вектора
    CPointVector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}
    // Конструктор для двумерного вектора
    CPointVector(double _x, double _y): x(_x), y(_y), z(0)
    {}
    CPointVector(): x(0), y(0), z(0)
    {}
    double x = 0;
    double y = 0;
    double z = 0;
    bool operator==(const CPointVector& other) const {
        return this->x == other.x && this->y == other.y && this->z == other.z;
    }
    bool operator!=(const CPointVector &other) const {
        return !(*this == other);
    }
    bool operator<(const CPointVector& other) const {
        if (this->z != other.z) {
            return this->z < other.z;
        }
        else if (this->x != other.x) {
            return this->x < other.x;
        }
        else {
            return this->y < other.y;
        }
    }
    CPointVector operator+(const CPointVector& other) const {
        return CPointVector(this->x + other.x, this->y + other.y, this->z + other.z);
    }
    CPointVector operator-(const CPointVector& other) const {
        return CPointVector(this->x - other.x, this->y - other.y, this->z - other.z);
    }
    CPointVector operator*(double multipiler) const {
        return CPointVector(this->x * multipiler, this->y * multipiler, this->z * multipiler);
    }
    CPointVector operator/(double divisor) const {
        return CPointVector(this->x / divisor, this->y / divisor, this->z / divisor);
    }
    void operator+=(const CPointVector& other) {
        this->x += other.x;
        this->y += other.y;
        this->z += other.z;
    }
    void operator-=(const CPointVector& other) {
        this->x -= other.x;
        this->y -= other.y;
        this->z -= other.z;
    }
    void operator*=(double multipiler) {
        this->x *= multipiler;
        this->y *= multipiler;
        this->z *= multipiler;
    }
    void operator/=(double divisor) {
        this->x /= divisor;
        this->y /= divisor;
        this->z /= divisor;
    }
    // Скалярное произведение
    double dot(const CPointVector &other) const {
        return this->x * other.x + this->y * other.y + this->z * other.z;
    }
    // Векторное произведение
    CPointVector cross(const CPointVector& other) const {
        double cross_x = this->y * other.z - this->z * other.y;
        double cross_y = this->z * other.x - this->x * other.z;
        double cross_z = this->x * other.y - this->y * other.x;
        return CPointVector(cross_x, cross_y, cross_z);
    }
    // Смешанное произведение
    double triple(const CPointVector& first, const CPointVector& second) const {
        return x * (first.y * second.z - first.z * second.y) - y * (first.x * second.z - second.x * first.z) +
        z * (first.x * second.y - second.x * first.y);
    }
    double length() const {
        return sqrt(x * x + y * y + z * z);
    }
    // Нормировка вектора
    CPointVector normalize() const {
        return *this / this->length();
    }
    
    void normalize() {
        *this /= this->length();
    }
};

// Компаратор, сравнивающий вектора по полярному углу
bool compareByPolar (const CPointVector& first, const CPointVector& second) {
    return atan2(first.y, first.x) > atan2(second.y, second.x);

}

// Класс, хранящий многоугольники
class CPolygon {
public:
    explicit CPolygon(const std::vector<CPointVector>& _vertices): m_vertices(_vertices), m_size(_vertices.size()),
                                                                   m_xMax(-std::numeric_limits<double>::max()),
                                                                   m_yMax(-std::numeric_limits<double>::max())
    {
        // Добавляем в массив соответствующие ребра и обновляем максимумы по x и y
        for (size_t i = 1; i <= m_size; ++i) {
            m_edges.push_back(m_vertices[i % m_size] - m_vertices[i - 1]);
            if (m_vertices[i % m_size].x > m_xMax) {
                m_xMax = m_vertices[i % m_size].x;
            }
            if (m_vertices[i % m_size].y > m_yMax) {
                m_yMax = m_vertices[i % m_size].y;
            }
        }

    }

    CPolygon(const std::vector<CPointVector>& _edges, const double _xMax, const double _yMax):
            m_edges(_edges), m_size(_edges.size()), m_vertices(_edges.size()), m_xMax(_xMax), m_yMax(_yMax) {
        double currentMax_x = -std::numeric_limits<double>::max(), currentMax_y = -std::numeric_limits<double>::max();
        // Строим многоугольник, начиная с вершины (0, 0) и обновляем максимумы по x и y для построенного многоугольника
        CPointVector currentVertex(0, 0);
        for (size_t i = 0; i < m_size; ++i) {
            m_vertices[i] = currentVertex;
            currentVertex += m_edges[i];
            if (m_vertices[i].x > currentMax_x) {
                currentMax_x = m_vertices[i].x;
            }
            if (m_vertices[i].y > currentMax_y) {
                currentMax_y = m_vertices[i].y;
            }
        }
        // Вектор, на который надо сместить многоугольник, чтобф значения максимумов совпали с переданными
        CPointVector delta(m_xMax - currentMax_x, m_yMax - currentMax_y);
        // Выполняем смещение
        for (auto&& vertex: m_vertices) {
            vertex += delta;
        }
    }

    size_t size() const {
        return m_size;
    }

    // Проверка точки на принадлежность выпуклому многоугольнику
    bool contains(const CPointVector& point) {
        for (size_t i = 1; i <= m_size; ++i) {
            if ((m_vertices[i % m_size] - m_vertices[i - 1]).x * (m_vertices[i - 1] - point).y -
                (m_vertices[i % m_size] - m_vertices[i - 1]).y * (m_vertices[i - 1] - point).x < 0) {
                return false;
            }
        }
        return true;
    }
    // Сумма Минковского двух выпуклых многоугольников
    CPolygon operator+(const CPolygon& other) const {
        // Определяем позиции ребер с минимальным полярным углом
        auto firstPos = std::min_element(m_edges.begin(), m_edges.end(), compareByPolar);
        auto secondPos = std::min_element(other.m_edges.begin(), other.m_edges.end(), compareByPolar);
        // Счетчики, отмечающие позицию вставки элемента
        int firstCnt = 0;
        int secondCnt = 0;
        std::vector<CPointVector> newEdges(m_size + other.size());
        // Выполняем слияние двух отсортированных массивов
        while(firstCnt != m_size && secondCnt != other.m_size) {
            if (compareByPolar(*firstPos, *secondPos)) {
                newEdges[firstCnt + secondCnt] = *firstPos;
                // Смещаем итератор циклически
                firstPos = firstPos == m_edges.end() - 1? m_edges.begin(): firstPos + 1;
                ++firstCnt;
            } else {
                newEdges[firstCnt + secondCnt] = *secondPos;
                secondPos = secondPos == other.m_edges.end() - 1 ? other.m_edges.begin() : secondPos + 1;
                ++secondCnt;
            }
            // Если дошли до конца одного из массивов, записываем остаток второго в конец результата слияния
            if (firstCnt == m_size) {
                while (secondCnt != other.m_size) {
                    newEdges[firstCnt + secondCnt] = *secondPos;
                    secondPos = secondPos == other.m_edges.end() - 1 ? other.m_edges.begin() : secondPos + 1;
                    ++secondCnt;
                }
            } else if (secondCnt == other.m_size) {
                while (firstCnt != m_size) {
                    newEdges[firstCnt + secondCnt] = *firstPos;
                    firstPos = firstPos == m_edges.end() - 1 ? m_edges.begin() : firstPos + 1;
                    ++firstCnt;
                }
            }
        }
        // По свойствам суммы Минковского максимумы нового прямоугольника по x и y равны сумме соответствующих максимумов у
        // исходных многоугольников
        return CPolygon(newEdges, m_xMax + other.m_xMax, m_yMax + other.m_yMax);
    }

private:
    std::vector<CPointVector> m_vertices;
    std::vector<CPointVector> m_edges;
    size_t m_size;
    double m_xMax;
    double m_yMax;
    friend void reversePolygon(CPolygon& poly);
};

// Отражение многоугольника относительно начала координат
void reversePolygon(CPolygon& poly) {
    poly.m_xMax = -std::numeric_limits<double>::max();
    poly.m_yMax = -std::numeric_limits<double>::max();
    // Умножаем вектора на -1 и обновляем максимумы
    for (int i = 0; i < poly.size(); ++i) {
        poly.m_vertices[i] *= -1;
        poly.m_edges[i] *= -1;
        if (poly.m_vertices[i].x > poly.m_xMax) {
            poly.m_xMax = poly.m_vertices[i].x;
        }
        if (poly.m_vertices[i].y > poly.m_yMax) {
            poly.m_yMax = poly.m_vertices[i].y;
        }
    }
}

// Проверка выпуклых многоугольников на пересечение
bool hasIntersection(const CPolygon& first, CPolygon& second) {
    reversePolygon(second);
    CPointVector zero(0, 0);
    // Многоугольники P и Q пересекаются <=> P + (-Q) содержит точку (0, 0)
    return (first + second).contains(zero);
}

int main() {
    size_t polySize;
    std::cin >> polySize;
    double x, y;
    std::vector<CPointVector> points;
    for (int i = 0; i < polySize; ++i) {
        std::cin >> x >> y;
        points.emplace_back(x, y);
    }
    CPolygon firstPoly(points);
    points.clear();
    std::cin >> polySize;
    for (int i = 0; i < polySize; ++i) {
        std::cin >> x >> y;
        points.emplace_back(x, y);
    }
    CPolygon secondPoly(points);
    if (hasIntersection(firstPoly, secondPoly)) {
        std::cout << "YES" << std::endl;
    }
    else {
        std::cout << "NO" << std::endl;
    }
    return 0;
}
