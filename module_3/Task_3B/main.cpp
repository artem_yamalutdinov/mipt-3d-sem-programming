#include <iostream>
#include <vector>
#include <cmath>
#include <stack>
#include <set>
#include <cstdio>
#include <algorithm>

// Класс, реализующий основные операции с векторами и точками в 2D и 3D
class CPointVector {
public:
    // Конструктор для трехмерного вектора
    CPointVector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}
    // Конструктор для двумерного вектора
    CPointVector(double _x, double _y): x(_x), y(_y), z(0)
    {}
    CPointVector(): x(0), y(0), z(0)
    {}
    double x = 0;
    double y = 0;
    double z = 0;
    bool operator==(const CPointVector& other) const {
        return this->x == other.x && this->y == other.y && this->z == other.z;
    }
    bool operator!=(const CPointVector &other) const {
        return !(*this == other);
    }
    bool operator<(const CPointVector& other) const {
        if (this->z != other.z) {
            return this->z < other.z;
        }
        else if (this->x != other.x) {
            return this->x < other.x;
        }
        else {
            return this->y < other.y;
        }
    }
    CPointVector operator+(const CPointVector& other) const {
        return CPointVector(this->x + other.x, this->y + other.y, this->z + other.z);
    }
    CPointVector operator-(const CPointVector& other) const {
        return CPointVector(this->x - other.x, this->y - other.y, this->z - other.z);
    }
    CPointVector operator*(double multipiler) const {
        return CPointVector(this->x * multipiler, this->y * multipiler, this->z * multipiler);
    }
    CPointVector operator/(double divisor) const {
        return CPointVector(this->x / divisor, this->y / divisor, this->z / divisor);
    }
    void operator+=(const CPointVector& other) {
        this->x += other.x;
        this->y += other.y;
        this->z += other.z;
    }
    void operator-=(const CPointVector& other) {
        this->x -= other.x;
        this->y -= other.y;
        this->z -= other.z;
    }
    void operator*=(double multipiler) {
        this->x *= multipiler;
        this->y *= multipiler;
        this->z *= multipiler;
    }
    void operator/=(double divisor) {
        this->x /= divisor;
        this->y /= divisor;
        this->z /= divisor;
    }
    // Скалярное произведение
    double dot(const CPointVector &other) const {
        return this->x * other.x + this->y * other.y + this->z * other.z;
    }
    // Векторное произведение
    CPointVector cross(const CPointVector& other) const {
        double cross_x = this->y * other.z - this->z * other.y;
        double cross_y = this->z * other.x - this->x * other.z;
        double cross_z = this->x * other.y - this->y * other.x;
        return CPointVector(cross_x, cross_y, cross_z);
    }
    // Смешанное произведение
    double triple(const CPointVector& first, const CPointVector& second) const {
        return x * (first.y * second.z - first.z * second.y) - y * (first.x * second.z - second.x * first.z) +
               z * (first.x * second.y - second.x * first.y);
    }
    double length() const {
        return sqrt(x * x + y * y + z * z);
    }
    // Нормировка вектора
    CPointVector normalize() const {
        return *this / this->length();
    }

    void normalize() {
        *this /= this->length();
    }
};


// Класс для построения трехмерной выпуклой оболочки
class ConvexHull3D {
public:
    explicit  ConvexHull3D(const std::vector<CPointVector>& _points);
    void print() {
        printf("%d\n", m_size);
        for (auto&& tr: m_hull) {
            printf("%d %d %d %d\n", 3, std::distance(m_points.begin(), tr.first),
                   std::distance(m_points.begin(), tr.second), std::distance(m_points.begin(), tr.third));
        }
    };

private:
    // Класс для хранения грани оболочки
    class Triangle {
        using PointIter = std::vector<CPointVector>::iterator;
    public:
        Triangle(PointIter _first, PointIter _second, PointIter _third): first(_first), second(_second), third(_third)
        {}
        bool operator == (const Triangle& other) const {
            return first == other.first && second == other.second && third == other.third;
        }
        // Лексикографический порядок
        bool operator < (const Triangle& other) const {
            if (first != other.first) {
                return first < other.first;
            }
            else if (second != other.second) {
                return second < other.second;
            }
            else {
                return third < other.third;
            }
        }
        PointIter first;
        PointIter second;
        PointIter third;
    };
    // Сортировка вершин треугольника относительно внешей нормали
    void sortTriangle(Triangle& tr) {
        // Помещаем на первое место вершину с наименьшим индексом
        if (tr.second < tr.first) {
            std::swap(tr.first, tr.second);
        }
        if (tr.third < tr.first) {
            std::swap(tr.third, tr.first);
        }
        CPointVector internalDirection = m_internalPoint - *tr.first;
        // Если с вектором, направленным вовнутрь оболочки, ребра образуют правую тройку, то относительно внешней нормали
        // они будут ориентированы против часовой стрелки
        if (internalDirection.triple(*tr.second - *tr.first, *tr.third - *tr.first) > 0) {
            std::swap(tr.second, tr.third);
        }
    }
    // Внутренняя точка (необходима для сортировки вершин в гранях
    CPointVector m_internalPoint;
    // Набор точек, для которых строится оболочка
    std::vector<CPointVector> m_points;
    // Грани оболочки, отсортированные лексикографически
    std::set<Triangle> m_hull;
    // Количество граней в оболочке
    size_t m_size;

    // Поворот вокруг ребра, проходящего через firstPt и secondPt
    auto pivot(const CPointVector& firstPt, const CPointVector& secondPt, const CPointVector& thirdPt) {
        double angle = 1;
        auto newPt = m_points.begin();
        for (auto pt = m_points.begin(); pt < m_points.end(); ++pt) {
            if (*pt != firstPt && *pt != secondPt && *pt != thirdPt) {
                // Ищем косинус угла между плоскостями как скалярное произведение единичных нормалей
                CPointVector firstNorm = (thirdPt - firstPt).cross(thirdPt - secondPt);
                firstNorm.normalize();
                CPointVector secondNorm = (*pt - firstPt).cross(*pt - secondPt);
                secondNorm.normalize();
                // В данном случае нам важен знак скалярного произведения
                if (firstNorm.dot(secondNorm) <= angle) {
                    angle = firstNorm.dot(secondNorm);
                    newPt = pt;
                }
            }
        }
        return newPt;
    }

    // Алгоритм "заворачивания подарка"
    void wrap(Triangle firstFacet) {
        // Помещаем в стек ребра первой грани
        std::stack<Triangle> st;
        st.emplace(firstFacet.second, firstFacet.first, firstFacet.third);
        st.emplace(firstFacet.third, firstFacet.second, firstFacet.first);
        st.emplace(firstFacet.first, firstFacet.third, firstFacet.second);
        // Добавляем в оболочку первую грань с отсортированными ребрами
        sortTriangle(firstFacet);
        m_hull.insert(firstFacet);
        // Пока стек не пуст, выполняем повороты вокруг ребер
        while (!st.empty()) {
            auto edge = st.top();
            st.pop();
            auto nextPoint = pivot(*edge.first, *edge.second, *edge.third);
            Triangle newFacet(edge.first, edge.second, nextPoint);
            sortTriangle(newFacet);
            // Если построенная гран еще не встречалась в оболочке, помещаем ее туда
            if (m_hull.find(newFacet) == m_hull.end()) {
                st.emplace(newFacet.third, newFacet.second, newFacet.first);
                st.emplace(newFacet.first, newFacet.third, newFacet.second);
                m_hull.insert(newFacet);
            }
        }
        m_size = m_hull.size();
    }

    // Вспомогательный метод для построения первой грани
    auto buildTriangle(const CPointVector& firstPt, const CPointVector& secondPt) {
        double angle = 0;
        auto nextPt = m_points.begin();
        for (auto pt = m_points.begin(); pt < m_points.end(); ++pt) {
            if (*pt != firstPt && *pt != secondPt) {
                // Проектируем точку на плоскость, перпендикулярную оси z и содержащую firstPt
                CPointVector projection(pt->x, pt->y, firstPt.z);
                // Ищем косинус угла между плоскостями как скалярное произведение единичных нормалей
                CPointVector firstNorm = (projection - firstPt).cross(projection - secondPt);
                firstNorm.normalize();
                CPointVector secondNorm = (*pt - firstPt).cross(*pt - secondPt);
                secondNorm.normalize();
                // Если нашли наибольший по модулю косинус, запоминаем точку
                if (fabs(firstNorm.dot(secondNorm)) >= angle) {
                    angle = fabs(firstNorm.dot(secondNorm));
                    nextPt = pt;
                }
            }
        }
        return nextPt;
    }
};


ConvexHull3D::ConvexHull3D(const std::vector<CPointVector>& _points): m_points(_points) {
    // В качестве внутренней точки берем центр масс данной нам системы точек
    for (auto&& pt: m_points) {
        m_internalPoint += pt;
    }
    m_internalPoint /= m_points.size();
    //Строим первую грань оболочки
    auto startPoint = std::min_element(m_points.begin(), m_points.end());
    auto secondPoint = m_points.end();
    auto thirdPoint = m_points.begin();
    // Ищем точку, имеющую теже координаты по y и z, как у первой точки
    for (auto nextPoint = m_points.begin(); nextPoint < m_points.end(); ++nextPoint) {
        if (nextPoint->x > startPoint->x && nextPoint->y == startPoint->y && nextPoint->z == startPoint->z) {
            secondPoint = nextPoint;
            break;
        }
    }
    // Если такая точка не нашлась, заводим фиктивную точку с координатами (x+1, y, z)
    if (secondPoint == m_points.end()) {
        CPointVector fictionalPoint = *startPoint + CPointVector(1, 0, 0);
        // Ищем вторую точку поворотом вокруг прямой, содержащей стартовую и фиктивную точку
        secondPoint = buildTriangle(*startPoint, fictionalPoint);
        // Ищем третью точку поворотом вокруг прямой, содержащей первую и вторую точки
        thirdPoint = pivot(*startPoint, *secondPoint, fictionalPoint);
    }
    // Если же мы нашли такую точку, выполняем поворот вокруг построенной прямой
    else {
        thirdPoint = buildTriangle(*startPoint, *secondPoint);
    }
    // Достраиваем оболочку
    wrap({startPoint, secondPoint, thirdPoint});

}

int main() {
    std::vector<CPointVector> points;
    double x, y, z;
    int testsNum;
    std::cin >> testsNum;
    size_t size = 0;
    for (size_t i = 0; i < testsNum; ++i) {
        std::cin >> size;
        for (int i = 0; i < size; ++i) {
            std::cin >> x >> y >> z;
            points.emplace_back(x, y, z);
        }
        ConvexHull3D hull(points);
        hull.print();
        points.clear();
    }
    return 0;
}