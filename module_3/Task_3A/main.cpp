#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

// Класс, реализующий основные операции с векторами и точками в 2D и 3D
class CPointVector {
public:
    // Конструктор для трехмерного вектора
    CPointVector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}
    // Конструктор для двумерного вектора
    CPointVector(double _x, double _y): x(_x), y(_y), z(0)
    {}
    CPointVector(): x(0), y(0), z(0)
    {}
    double x = 0;
    double y = 0;
    double z = 0;
    bool operator==(const CPointVector& other) const {
        return this->x == other.x && this->y == other.y && this->z == other.z;
    }
    bool operator!=(const CPointVector &other) const {
        return !(*this == other);
    }
    bool operator<(const CPointVector& other) const {
        if (this->z != other.z) {
            return this->z < other.z;
        }
        else if (this->x != other.x) {
            return this->x < other.x;
        }
        else {
            return this->y < other.y;
        }
    }
    CPointVector operator+(const CPointVector& other) const {
        return CPointVector(this->x + other.x, this->y + other.y, this->z + other.z);
    }
    CPointVector operator-(const CPointVector& other) const {
        return CPointVector(this->x - other.x, this->y - other.y, this->z - other.z);
    }
    CPointVector operator*(double multipiler) const {
        return CPointVector(this->x * multipiler, this->y * multipiler, this->z * multipiler);
    }
    CPointVector operator/(double divisor) const {
        return CPointVector(this->x / divisor, this->y / divisor, this->z / divisor);
    }
    void operator+=(const CPointVector& other) {
        this->x += other.x;
        this->y += other.y;
        this->z += other.z;
    }
    void operator-=(const CPointVector& other) {
        this->x -= other.x;
        this->y -= other.y;
        this->z -= other.z;
    }
    void operator*=(double multipiler) {
        this->x *= multipiler;
        this->y *= multipiler;
        this->z *= multipiler;
    }
    void operator/=(double divisor) {
        this->x /= divisor;
        this->y /= divisor;
        this->z /= divisor;
    }
    // Скалярное произведение
    double dot(const CPointVector &other) const {
        return this->x * other.x + this->y * other.y + this->z * other.z;
    }
    // Векторное произведение
    CPointVector cross(const CPointVector& other) const {
        double cross_x = this->y * other.z - this->z * other.y;
        double cross_y = this->z * other.x - this->x * other.z;
        double cross_z = this->x * other.y - this->y * other.x;
        return CPointVector(cross_x, cross_y, cross_z);
    }
    // Смешанное произведение
    double triple(const CPointVector& first, const CPointVector& second) const {
        return x * (first.y * second.z - first.z * second.y) - y * (first.x * second.z - second.x * first.z) +
               z * (first.x * second.y - second.x * first.y);
    }
    double length() const {
        return sqrt(x * x + y * y + z * z);
    }
    // Нормировка вектора
    CPointVector normalize() const {
        return *this / this->length();
    }

    void normalize() {
        *this /= this->length();
    }

    bool isZero() const {
        return x == 0 && y == 0 && z == 0;
    }
};

class CSegment {
public:
    CSegment (const CPointVector& _m_begin, const CPointVector& _end): m_begin(_m_begin), m_direction(_end - _m_begin)
    {}

    const CPointVector& begin() const {
        return m_begin;
    }

    const CPointVector& direction() const {
        return m_direction;
    }

    const CPointVector& end() const {
        return m_begin + m_direction;
    }

    double minDistance(const CSegment& other);

private:
    CPointVector m_begin;
    CPointVector m_direction;
};

// Проверка направляющих векторов на коллинеарность
bool areCollinear(const CSegment &firstSeg, const CSegment &secondSeg) {
    return firstSeg.direction().cross(secondSeg.direction()).length() < 1e-6;
}

double CSegment::minDistance(const CSegment& other) {
    // Проверка на вырожденность
    if (m_direction.isZero() && other.direction().isZero()) {
        return (m_begin - other.begin()).length();
    }
    // Вычисляем вспомогательные скалярные произведения
    double firstSqLength = m_direction.dot(m_direction);
    double secondSqLength = other.direction().dot(other.direction());
    double directionsDot = m_direction.dot(other.direction());
    double firstBeginDot = m_direction.dot(m_begin - other.begin());
    double secondBeginDot = other.direction().dot(m_begin - other.begin());
    // Коэффициент перед направляющим вектором, соответствующий первому отрезку
    double firstCoeff = 0;
    // Коэффициент перед направляющим вектором, соответствующий второму отрезку
    double secondCoeff = 0;
    // Рассмотрим случай, когда направляющие векторы коллинеарны и второй отрезок невырожден
    if (areCollinear(*this, other) && secondSqLength != 0) {
        firstCoeff = 0;
        secondCoeff = secondBeginDot / secondSqLength;
    } else {
        // Вычисляем значения коэффициентов, соответствующие наиболее близким точкам на прямой
        double crossLength = m_direction.cross(other.direction()).length();
        // Избегем потенциального деления на 0
        if (crossLength == 0) {
            crossLength = 1;
        }
        firstCoeff = (directionsDot * secondBeginDot - secondSqLength * firstBeginDot) / (crossLength * crossLength);
        secondCoeff = (firstSqLength * secondBeginDot - directionsDot * firstBeginDot) / (crossLength * crossLength);
        // Если первый коэффициент меньше или равен 0, ищем минимум вдоль прямой firstCoeff = 0
        if (firstCoeff <= 0) {
            firstCoeff = 0;
            // Проверим, не вырожден ли второй отрезок
            secondCoeff = secondSqLength == 0 ? 0: secondBeginDot / secondSqLength;
        }
            // Если первый коэффициент больше или равен 1, ищем минимум вдоль прямой firstCoeff = 1
        else if (firstCoeff >= 1) {
            firstCoeff = 1;
            // Проверим, не вырожден ли второй отрезок
            secondCoeff = secondSqLength == 0 ? 0: (secondBeginDot + directionsDot) / secondSqLength;
        }
        // Выполняем аналогичные сравнения для второго коэффициента
        if (secondCoeff <= 0) {
            secondCoeff = 0;
            // Проверим, лежит ли найденный нами минимум первого коэффициента в отрезке [0; 1]
            if (firstBeginDot >= 0) {
                firstCoeff = 0;
            } else if (firstBeginDot <= -firstSqLength) {
                firstCoeff = 1;
            } else {
                firstCoeff = -firstBeginDot / firstSqLength;
            }
        } else if (secondCoeff >= 1) {
            secondCoeff = 1;
            if (directionsDot <= firstBeginDot) {
                firstCoeff = 0;
            } else if (directionsDot >= firstBeginDot + firstSqLength) {
                firstCoeff = 1;
            } else {
                firstCoeff = (directionsDot - firstBeginDot) / firstSqLength;
            }

        }
    }
    return ((m_begin - other.begin()) + m_direction * firstCoeff - other.direction() * secondCoeff).length();
}

int main() {
    CPointVector firstBegin, firstEnd;
    std::cin >> firstBegin.x >> firstBegin.y >> firstBegin.z;
    std::cin >> firstEnd.x >> firstEnd.y >> firstEnd.z;
    CSegment firstSegment(firstBegin, firstEnd);
    CPointVector secondBegin, secondEnd;
    std::cin >> secondBegin.x >> secondBegin.y >> secondBegin.z;
    std::cin >> secondEnd.x >> secondEnd.y >> secondEnd.z;
    CSegment secondSegment(secondBegin, secondEnd);
    std::cout << std::fixed << std::setprecision(10) << firstSegment.minDistance(secondSegment) << std::endl;
    return 0;
}