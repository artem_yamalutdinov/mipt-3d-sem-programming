#include<iostream>
#include <cassert>
#include <vector>
#include <string>
#include "biginteger.h"

int main() {
    BigInteger bi1;
    BigInteger bi2;
    std::cin >> bi1 >> bi2;
    std::cout << bi1 / bi2;
    return 0;
}