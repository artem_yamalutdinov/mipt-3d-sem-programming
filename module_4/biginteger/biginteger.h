//
// Created by Артем Ямалутдинов on 04/12/2018.
//

#ifndef BIGINTEGER_BIGINTEGER_H
#define BIGINTEGER_BIGINTEGER_H

#include <vector>
#include <string>
#include <iostream>

class BigInteger {
public:
    BigInteger(): m_isPositive(true), m_digits(1, 0)
    {}
    BigInteger(long long _value): m_isPositive(_value >= 0) {
        long long tmpValue = m_isPositive ? _value : -_value;
        do {
            m_digits.push_back(tmpValue % BASE);
            tmpValue /= BASE;
        } while (tmpValue > 0);
    }

    BigInteger(const std::string& strValue) {
        // Пустую строку будем воспринимать как 0
        if (strValue.empty()) {
            m_isPositive = true;
            m_digits.push_back(0);
        }
        else {
            m_isPositive = true;
            // false, если первый символ – цифра, true, если первый символ – минус
            bool startsWithMinus = false;
            if (strValue[startsWithMinus] == '-') {
                m_isPositive = false;
            }
            //Проходимся циклом либо до конца строки, либо до знака -
            for (long long i = strValue.length(); i > startsWithMinus; i -= 9) {
                long long currentPos = std::max(static_cast<long long>(startsWithMinus), i - 9);
                long long digitsNum = std::min(i - startsWithMinus, 9LL);
                m_digits.push_back(std::stoi(strValue.substr(currentPos, digitsNum)));
            }
            //Удаляем нули в первых разрядах, если они есть
            this->normalize();
        }
    }

    BigInteger& operator =(const BigInteger& other) = default;

    BigInteger& operator= (int value) {
        *this = BigInteger(value);
        return *this;
    }

    const BigInteger operator -() const {
        BigInteger newNumber(*this);
        // Для нуля знак не меняется
        if (newNumber != 0) {
            newNumber.m_isPositive ^= true;
        }
        return newNumber;
    }

    bool operator ==(const BigInteger& other) const {
        if (m_digits.size() != other.m_digits.size() || m_isPositive != other.m_isPositive) {
            return false;
        }
        else {
            for (size_t i = 0; i < m_digits.size(); ++i) {
                if (m_digits[i] != other.m_digits[i]) {
                    return false;
                }
            }
        }
        return true;
    }

    bool operator !=(const BigInteger& other) const {
        return !(*this == other);
    }

    bool operator <(const BigInteger& other) const {
        if (this->m_isPositive && !other.m_isPositive) {
            return false;
        }
        if (other.m_isPositive && !this->m_isPositive) {
            return true;
        }
        // *this < other <=> -other < -*this
        BigInteger left = this->m_isPositive ? *this : -other;
        BigInteger right = other.m_isPositive ? other : -*this;
        if (left.digitsNum() != right.digitsNum()) {
            return left.digitsNum() < right.digitsNum();
        }
        else {
            for (long long i = left.digitsNum() - 1; i >= 0; --i) {
                if (left.m_digits[i] != right.m_digits[i]) {
                    return left.m_digits[i] < right.m_digits[i];
                }
            }
        }
        return false;
    }

    bool operator <=(const BigInteger& other) const {
        return *this < other || *this == other;
    }

    bool operator >(const BigInteger& other) const {
        return !(*this <= other);
    }

    bool operator >=(const BigInteger& other) const {
        return !(*this < other);
    }

    BigInteger& operator +=(const BigInteger& other) {
        //Перебираем случаи разных знаков
        if (!this->m_isPositive) {
            *this = -*this;
            if (other.m_isPositive) {
                *this -= other;
            }
            else {
                *this += (-other);
            }
            *this = -*this;
        }
        else if (!other.m_isPositive) {
            *this -= (-other);
        }
        else {
            //Если один из разрядов переполняется, переносим 1 в следующий
            bool overflow = false;
            for (size_t i = 0; i < std::max(this->digitsNum(), other.digitsNum()) || overflow; ++i) {
                if (i == digitsNum()) {
                    m_digits.push_back(0);
                }
                this->m_digits[i] += (i < other.digitsNum() ? other.m_digits[i] + overflow : overflow);
                overflow = this->m_digits[i] >= BASE;
                if (overflow) {
                    this->m_digits[i] -= BASE;
                }
            }
        }
        return *this;
    }

    BigInteger& operator -=(const BigInteger& other) {
        // Случаи для отрицательных чисел
        if (this->m_isPositive && !other.m_isPositive) {
            *this += (-other);
        }
        else if (!this->m_isPositive && other.m_isPositive) {
            *this = -*this;
            *this += other;
            *this = -*this;
        }
        else if (!this->m_isPositive && !other.m_isPositive) {
            *this = -(-*this - -other);
        }
        else if (*this < other)  {
            *this = -(other - *this);
        }
        else {
            bool overflow = false;
            for (size_t i = 0; i < std::max(this->digitsNum(), other.digitsNum()) || overflow; ++i) {
                this->m_digits[i] -= (i < other.digitsNum() ? other.m_digits[i] + overflow : overflow);
                overflow = this->m_digits[i] < 0;
                if (overflow) {
                    this->m_digits[i] += BASE;
                }
            }
        }
        this->normalize();
        return *this;
    }

    // Префиксный инкремент
    BigInteger& operator ++() {
        return *this += 1;
    }

    // Постфиксный инкремент
    const BigInteger operator ++(int) {
        BigInteger tmp = *this;
        *this += 1;
        return tmp;
    }

    BigInteger& operator --() {
        return *this -= 1;
    }

    const BigInteger operator --(int) {
        BigInteger tmp = *this;
        *this -= 1;
        return tmp;
    }

    BigInteger& operator *= (const BigInteger& other) {
        BigInteger result;
        result.m_isPositive = !(this->m_isPositive ^ other.m_isPositive);
        // Число разрядов в результате не превосходит суммы числа разрядов множителей
        result.m_digits.assign(this->digitsNum() + other.digitsNum(), 0);
        for (size_t j = 0; j < other.digitsNum(); ++j) {
            int overflow = 0;
            // Умножаем. пока не дойдем до конца массива либо пока есть
            for (size_t i = 0; i < this->digitsNum() || overflow != 0; ++i) {
                size_t multipiler = result.m_digits[i + j] + overflow;
                multipiler += i < this->digitsNum() ? static_cast<size_t>(this->m_digits[i]) * other.m_digits[j] : 0LL;
                result.m_digits[i + j] = multipiler % BASE;
                overflow = multipiler / BASE;
            }
        }
        *this = result;
        // Удаляем ведущие нули
        this->normalize();
        return *this;
    }

    BigInteger& operator /=(const BigInteger& other) {
        if (other == 0) {
            throw std::runtime_error("Division by zero");
        }
        BigInteger divisor = other < 0 ? -other : other;
        BigInteger result, remain;
        result.m_isPositive = !(this->m_isPositive ^ other.m_isPositive);
        result.m_digits.assign(this->digitsNum(), 0);
        this->m_isPositive = true;
        for (long long i = this->digitsNum() - 1; i >=0; --i) {
            remain.swipeRight();
            remain.m_digits[0] = this->m_digits[i];
            remain.normalize();
            int multiplier = 0;
            // Ищем максимальное число, которое можно вычесть, с помощю бинарного поиска
            int lBound = 0, rBound = BASE;
            while (lBound <= rBound) {
                int middle = (lBound + rBound) / 2;
                BigInteger toSubtract = divisor * middle;
                if (toSubtract <= remain) {
                    lBound = middle + 1;
                    multiplier = middle;
                }
                else {
                    rBound = middle - 1;
                }
            }
            result.m_digits[i] = multiplier;
            remain -= divisor * multiplier;
        }
        *this = result;
        return this->normalize();
    }


    // Знак остатка совпадает со знаком делимого
    BigInteger& operator %=(const BigInteger& other) {
        *this -= (*this / other) * other;
        return *this;
    }


    std::string toString() const {
        std::string res;
        if (!m_isPositive) {
            res += '-';
        }
        for (long long i = this->digitsNum() - 1; i >= 0; --i) {
            std::string digitsPart = std::to_string(m_digits[i]);
            if (i != static_cast<long long>(digitsNum()) - 1) {
                // Дозаполняем строку нулями
                for (int j = digitsPart.length(); j < 9; ++j) {
                    res += '0';
                }
            }
            res += digitsPart;
        }
        return res;
    }

    explicit operator bool() const {
        return *this != 0;
    }

    explicit operator int() const {
        const int MAX = 2147483647;
        size_t value = m_digits[1] * BASE + m_digits[0];
        if (value > MAX) {
            return -1;
        }
        else {
            int result = m_isPositive ? 1 : -1;
            result *= static_cast<int>(value);
            return result;
        }
    }

    friend const BigInteger operator +(const BigInteger& left, const BigInteger& right);

    friend const BigInteger operator -(const BigInteger& left, const BigInteger& right);

    friend const BigInteger operator *(const BigInteger& left, const BigInteger& right);

    friend const BigInteger operator /(const BigInteger& left, const BigInteger& right);

    friend const BigInteger operator %(const BigInteger& left, const BigInteger& right);

private:
    static const int BASE = 1e9;
    bool m_isPositive;
    std::vector<int> m_digits;

    size_t digitsNum() const {
        return m_digits.size();
    }

    // Приведение числа к нормальной записи
    BigInteger& normalize() {
        // Удаляем нули в старших разрядах
        while (this->digitsNum() > 1 && this->m_digits.back() == 0) {
            this->m_digits.pop_back();
        }
        // Меняем знак нуля на положительный
        if (this->m_digits.back() == 0) {
            m_isPositive = true;
        }
        return *this;
    }

    //Сдвиг разрядов вправо
    BigInteger& swipeRight() {
        m_digits.insert(m_digits.begin(), 0);
        return *this;
    }
};

std::istream& operator >>(std::istream& in, BigInteger& num) {
    std::string str;
    in >> str;
    num = BigInteger(str);
    return in;
}

std::ostream& operator <<(std::ostream& out, const BigInteger& num) {

    out << num.toString();
    return out;
}

const BigInteger operator +(const BigInteger& left, const BigInteger& right) {
        BigInteger res = left;
        return res += right;
    }

const BigInteger operator -(const BigInteger& left, const BigInteger& right) {
    BigInteger res = left;
    return res -= right;
}

const BigInteger operator *(const BigInteger& left, const BigInteger& right) {
        BigInteger res = left;
        return res *= right;
    }

const BigInteger operator /(const BigInteger& left, const BigInteger& right) {
        BigInteger res = left;
        return res /= right;
    }

const BigInteger operator %(const BigInteger& left, const BigInteger& right) {
        BigInteger res = left;
        return res %= right;
    }


#endif //BIGINTEGER_BIGINTEGER_H
